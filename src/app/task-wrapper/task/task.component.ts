import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Taski } from 'src/app/interfaces/taski.interface';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() task!: Taski;

  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
  }

  public onCompleteTask(): void {
    this.task.isDone = !this.task.isDone;
    console.log(this.task);
  }

  public onDeleteTask(): void {
    this.taskService.deleteTask(this.task.id);
  }

}
