import { Component, OnInit } from '@angular/core';
import { Taski } from '../interfaces/taski.interface';
import { TaskService } from '../services/task.service';

@Component({
  selector: 'app-task-wrapper',
  templateUrl: './task-wrapper.component.html',
  styleUrls: ['./task-wrapper.component.css']
})
export class TaskWrapperComponent implements OnInit {

  newTaskName!: string;

  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
  }

  get taskList(): Taski[] {
    return this.taskService.taskList;
  }

  public onAddNewTask(): void {
    this.taskService.addTask(this.newTaskName);
    this.newTaskName = "";
  }

}
