export interface Taski {
    name: string;
    isDone: boolean;
    id: number;
}