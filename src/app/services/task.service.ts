import { Injectable } from '@angular/core';
import { Taski } from '../interfaces/taski.interface';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  taskList: Taski[] = [];
  private taskId: number = 0;

  constructor() { }

  public addTask(taskName: string): void {
    const task: Taski = { name: taskName, isDone: false, id: this.taskId };
    this.taskList.push(task);
    this.taskId++;
  }

  public deleteTask(id: number): void {
    this.taskList = this.taskList.filter(x => x.id != id);
    console.log(id);
  }
}
